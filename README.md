#                        JUEGO TENIS- Micaela Amaya

## Instrucciones del juego
 - Para mover la raqueta 1, se utilizan las teclas 'w' y 's'
 - Para la segunda con 'o' y 'l'
 
## Objetivo
 Consiste en que cada jugador consiga que la pelota choque con la pared contrincante. Si lo hace, se suma un punto al marcador, y así durante todo el juego. Gana quien tenga más puntos.
