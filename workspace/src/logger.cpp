#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>

#include "Puntuacion.h"

//LECTOR DEL FIFO

int main (void)
{
    int ret;
    int fd; // descripto fichero, para abrir tuberia
    Puntuacion punt; // Se lee de la tuberi­a una estructura con las puntuaciones

    ret = mkfifo("FIFO", 0666); //creamos el fifo

    fd = open ("FIFO", O_RDONLY); //abrimos en modo lectura
    if (fd < 0){
       perror ("error en open");
       return 1;
    }


    while(1) {
         if (read (fd, &punt, sizeof(punt)) < sizeof(punt)){
           perror("error en la lectura");
           break;
         }

         else{
           if (punt.ultiPunto == 1){
             printf ("El jugador 1 marca 1 punto, lleva un total de %d puntos \n", punt.puntosJ1);
             if (punt.puntosJ1 == 3){
               printf("Fin de la partida. Ha ganado el Jugador1 \n");
           break;
             }
           }
           if (punt.ultiPunto== 2){
             printf ("El jugador 2 marca 1 punto, lleva un total de %d puntos \n", punt.puntosJ2);
             if (punt.puntosJ2 == 3){
               printf("Fin de la partida. Ha ganado el Jugador2 \n");

           break;
             }       
           }
         }
    }

    close(fd);
    unlink ("FIFO");
    return (0);
    }

