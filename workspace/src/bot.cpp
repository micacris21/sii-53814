#include "DatosMemCompartida.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fstream>
#include "glut.h"

int main(){
	int fd;
	DatosMemCompartida *pDatos;
	fd = open("datosCompartidos", O_RDWR); //abrimos el fichero de proyeccion
	if (fd < 0){
	  perror ("Error en la apertura del fichero");
	  return 1;
	}
	
	pDatos = static_cast<DatosMemCompartida*>(mmap (NULL, sizeof(DatosMemCompartida), PROT_WRITE | PROT_READ, MAP_SHARED, fd, 0)); //proyección
	
	
	//control del bot
	while (1){
	  if (pDatos -> esfera.centro.y == pDatos -> raqueta1.GetCentro().y){
	    pDatos -> accion = 0;
	  }
	  if (pDatos -> esfera.centro.y < pDatos -> raqueta1.GetCentro().y){
	    pDatos -> accion = -1;
	  }
	  if (pDatos -> esfera.centro.y > pDatos -> raqueta1.GetCentro().y){
	    pDatos -> accion = 1;
	  }
	  
	  usleep(25000);
	}
	
	munmap(pDatos, sizeof(DatosMemCompartida));
	unlink("datosCompartidos");
	
	return 0;
}
